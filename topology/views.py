from django.shortcuts import render
from django.template.loader import render_to_string
from django.forms.models import model_to_dict
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from nuaal_api.models import Site, Link, Device
from django.http import Http404
import json
import os
from copy import deepcopy
# Create your views here.

def site_topology(request, siteId):
    site = None
    try:
        site = Site.objects.get(siteShortName=siteId)
    except ObjectDoesNotExist as e:
        return Http404
    device_qs = site.device_set.all()
    links_qs = []
    for device in device_qs:
        source_links = device.sourceDevice.all()
        target_links = device.targetDevice.all()
        for link in source_links:
            if link not in links_qs:
                links_qs.append(link)
        for link in target_links:
            if link not in links_qs:
                links_qs.append(link)
    context = {"nodes": device_qs, "links": links_qs}
    content_string = render_to_string(template_name="topology/data_js.jinja2", context=context)
    filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "static", "topology", "data.js")
    with open(file=filepath, mode="w") as f:
        f.write(content_string)
    return render(request=request, template_name='topology/topology.html', context=context)

def topology(request):
    noAps = bool(request.GET.get("noAps", False))
    device_qs = None
    if noAps:
        device_qs = Device.objects.exclude(deviceFamily="accesspoint")
    else:
        device_qs = Device.objects.all()
    links_qs = Link.objects.all()
    context = {"nodes": device_qs, "links": links_qs}
    content_string = render_to_string(template_name="topology/data_js.jinja2", context=context)
    filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "static", "topology", "data.js")
    with open(file=filepath, mode="w") as f:
        f.write(content_string)
    return render(request=request, template_name='topology/topology.html', context=context)
