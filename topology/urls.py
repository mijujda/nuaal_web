from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from topology import views


urlpatterns = [
    path('', views.topology),
    path('<siteId>/', views.site_topology)
]

urlpatterns = format_suffix_patterns(urlpatterns)