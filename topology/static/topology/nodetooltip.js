(function (nx) {
	// node tooltip class
	// see nx.graphic.Topology.Node reference to learn what node's properties you're able to use
	nx.define('TooltipNode', nx.ui.Component, {
		properties: {
			node: {}, // NeXt automatically provides you the access to the selected nx.graphic.Topology.Node instance
			topology: {}, // NeXt also provides you the access to the topology object
			url_link: {
				dependencies: ['node'],
				value: function(node) {
					if (node) {
                        return "/admin/nuaal_api/device/" + node.id()
                    }
				}
			}
		},
		// 'view' defines the appearance of the tooltip
		view: {
			content: {
				content: [
					{
						tag: 'h3',
						content: '{#node.label}'
					},
                    {
                        tag: 'p',
                        content: [
                            {
                                tag: 'label',
                                content: 'Site: '

                            },
                            {
                                tag: 'span',
                                content: '{#node.model.site}',
                                props: {
                                    'class': 'bold-text'
                                }
                            }
                        ]
                    },
                    {
                        tag: 'p',
                        content: [
                            {
                                tag: 'label',
                                content: 'Platform: '

                            },
                            {
                                tag: 'span',
                                content: '{#node.model.platform}',
                                props: {
                                    'class': 'bold-text'
                                }
                            }
                        ]
                    },
                    {
                        tag: 'p',
                        content: [
                            {
                                tag: 'label',
                                content: 'Device Type: '

                            },
                            {
                                tag: 'span',
                                content: '{#node.model.deviceType}',
                                props: {
                                    'class': 'bold-text'
                                }
                            }
                        ]
                    },
                    {
                        tag: 'p',
                        content: [
                            {
                                tag: 'label',
                                content: 'Interface Count: '

                            },
                            {
                                tag: 'span',
                                content: '{#node.model.interfaceCount}',
                                props: {
                                    'class': 'bold-text'
                                }
                            }
                        ]
                    },
					{
						tag: 'p',
						content: {
							tag: 'a',
							content: 'Admin',
							props: {
								href: '{#url_link}',
								'target': '_blank'
							}
						}
					}
                ],
				// applies to the whole tooltip box
				props: {
					// css class; see ./css/custom.css
					'class': 'custom-tooltip'
				}
			}
		}
	});
	// link tooltip class
	// see nx.graphic.Topology.Link reference to learn what link's properties you're able to use
	nx.define('TooltipLink', nx.ui.Component, {
		properties: {
			link: {},
			topology: {},
			url_link: {
				dependencies: ['link'],
				value: function(link) {
					if (link) {
                        return "/admin/nuaal_api/link/" + link.id()
                    }
				}
			}
		},
		view: {
			content: {
				content: [
                    {
                        tag: 'p',
                        content: [
                            {
                                tag: 'label',
                                content: 'Source Device: '

                            },
                            {
                                tag: 'span',
                                content: '{#link.model.sourceDevice}',
                                props: {
                                    'class': 'bold-text'
                                }
                            }
                        ]
                    },
                    {
                        tag: 'p',
                        content: [
                            {
                                tag: 'label',
                                content: 'Target Device: '

                            },
                            {
                                tag: 'span',
                                content: '{#link.model.targetDevice}',
                                props: {
                                    'class': 'bold-text'
                                }
                            }
                        ]
                    },
                    {
                        tag: 'p',
                        content: [
                            {
                                tag: 'label',
                                content: 'Source Interface: '

                            },
                            {
                                tag: 'span',
                                content: '{#link.model.sourceInterface}',
                                props: {
                                    'class': 'bold-text'
                                }
                            }
                        ]
                    },
                    {
                        tag: 'p',
                        content: [
                            {
                                tag: 'label',
                                content: 'Target Interface: '

                            },
                            {
                                tag: 'span',
                                content: '{#link.model.targetInterface}',
                                props: {
                                    'class': 'bold-text'
                                }
                            }
                        ]
                    },
					{
                        tag: 'p',
                        content: [
                            {
                                tag: 'label',
                                content: 'Link Type: '

                            },
                            {
                                tag: 'span',
                                content: '{#link.model.linkType}',
                                props: {
                                    'class': 'bold-text'
                                }
                            }
                        ]
                    },
					{
						tag: 'p',
						content: {
							tag: 'a',
							content: 'Admin',
							props: {
								href: '{#url_link}',
								'target': '_blank'
							}
						}
					}
                ],
				// applies to the whole tooltip box
				props: {
					// css class; see ./css/custom.css
					'class': 'custom-tooltip'
				}
			}
		}
	})
	;
})
(nx);