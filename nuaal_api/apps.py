from django.apps import AppConfig


class NuaalApiConfig(AppConfig):
    name = 'nuaal_api'
