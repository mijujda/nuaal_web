from django.db import models
import uuid
import socket
from nuaal.utils import int_name_convert
# Create your models here.


class Site(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    siteShortName = models.CharField(max_length=30, default="")
    siteLongName = models.CharField(max_length=30, default="")
    address = models.CharField(max_length=120, default="")

    def __str__(self):
        return "{} ({})".format(self.siteLongName, self.siteShortName)


class Device(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    hostname = models.CharField(max_length=30, default="")
    managementIp = models.CharField(max_length=15, default="")
    vendor = models.CharField(max_length=30, default="")
    platform = models.CharField(max_length=30, default="")
    sw = models.CharField(max_length=30, default="")
    swVersion = models.CharField(max_length=30, default="")
    site = models.ForeignKey(Site, on_delete=models.CASCADE)
    role = models.CharField(max_length=30, default="unknown", choices=(
        ("host", "Host"),
        ("accesspoint", "Access Point"),
        ("access", "Access"),
        ("distribution", "Distribution"),
        ("core", "Core"),
        ("unknown", "Unknown")
    ))
    deviceFamily = models.CharField(max_length=30, default="unknown", choices=(
        ("switch", "Switch"),
        ("router", "Router"),
        ("accesspoint", "Access Point"),
        ("unknown", "Unknown"),
    ))
    deviceType = models.CharField(max_length=30, default="unknown", choices=(
        ("fix", "Fix"),
        ("modular", "Modular"),
        ("stack", "Stack"),
        ("accesspoint", "Access Point"),
        ("unknown", "Unknown"),
    ))
    interfaceCount = models.IntegerField(default=0)
    eoxStatus = models.CharField(max_length=30, default="unknown", choices=(
        ("EoL", "End of Life"),
        ("OK", "No EoX"),
        ("unknown", "Unknown")
    ))

    def __str__(self):
        return "{} ({}/{})".format(self.hostname, self.vendor, self.platform)

    def _get_links(self):
        return self.sourceDevice.all() | self.targetDevice.all()

    def _get_link_count(self):
        return len(self._get_links())

    def get_neighbors(self):
        links = self._get_links()
        neighbors = []
        for link in links:
            if link.sourceDevice not in neighbors:
                neighbors.append(link.sourceDevice)
            if link.targetDevice not in neighbors:
                neighbors.append(link.targetDevice)
        try:
            neighbors.remove(self)
        except ValueError:
            pass
        finally:
            return neighbors

    def get_neighbors_count(self):
        return len(self.get_neighbors())

class Link(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sourceDevice = models.ForeignKey(Device, related_name="sourceDevice", on_delete=models.CASCADE)
    sourceInterface = models.CharField(max_length=20, default="")
    targetDevice = models.ForeignKey(Device, related_name="targetDevice", on_delete=models.CASCADE)
    targetInterface = models.CharField(max_length=20, default="")
    linkType = models.CharField(max_length=10, default="unknown", choices=(
        ("base-t", "Base-T"),
        ("multimode", "Multi-Mode"),
        ("singlemode", "Single-Mode"),
        ('twinax', "Twinax"),
        ("unknown", "Unknown")
    ))

    def get_interfaces(self):
        return self.sourceDevice.interface_set.filter(nameLong=self.sourceInterface) | self.targetDevice.interface_set.filter(nameLong=self.targetInterface)

    def get_sourceInterface(self):
        try:
            return self.sourceDevice.interface_set.get(nameLong=self.sourceInterface)
        except Exception as e:
            print(repr(e))
            return None

    def __str__(self):
        return "{} ({}) - {} ({})".format(
            int_name_convert(self.sourceInterface),
            self.sourceDevice.hostname,
            int_name_convert(self.targetInterface),
            self.targetDevice.hostname
        )


class Interface(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nameLong = models.CharField(max_length=30, default="")
    nameShort = models.CharField(max_length=30, default="")
    description = models.CharField(max_length=30, default="")
    interfaceFamily = models.CharField(max_length=30, default="unknown", choices=(
        ("physical", "Physical"),
        ("virtual", "Virtual"),
        ("etherchannel", "EtherChannel"),
        ("unknown", "Unknown")
    ))
    interfaceType = models.CharField(max_length=30, default="unknown")
    device = models.ForeignKey(Device, on_delete=models.CASCADE)

    def __str__(self):
        return "{} on {}".format(self.nameLong, str(self.device.hostname))


class Inventory(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    name = models.CharField(max_length=120, default="")
    desc = models.CharField(max_length=120, default="")
    pid = models.CharField(max_length=40, default="")
    vid = models.CharField(max_length=40, default="")
    sn = models.CharField(max_length=40, default="")

