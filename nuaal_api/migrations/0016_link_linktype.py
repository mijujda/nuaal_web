# Generated by Django 2.1.2 on 2018-10-23 22:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nuaal_api', '0015_auto_20181024_0005'),
    ]

    operations = [
        migrations.AddField(
            model_name='link',
            name='linkType',
            field=models.CharField(choices=[('base-t', 'Base-T'), ('multimode', 'Multi-Mode'), ('singlemode', 'Single_Mode')], default='unknown', max_length=10),
        ),
    ]
