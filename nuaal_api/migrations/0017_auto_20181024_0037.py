# Generated by Django 2.1.2 on 2018-10-23 22:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nuaal_api', '0016_link_linktype'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='linkType',
            field=models.CharField(choices=[('base-t', 'Base-T'), ('multimode', 'Multi-Mode'), ('singlemode', 'Single_Mode'), ('unknown', 'Unknown')], default='unknown', max_length=10),
        ),
    ]
