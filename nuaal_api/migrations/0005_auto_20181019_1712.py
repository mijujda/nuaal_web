# Generated by Django 2.1.1 on 2018-10-19 15:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nuaal_api', '0004_site_address'),
    ]

    operations = [
        migrations.AlterField(
            model_name='device',
            name='deviceFamily',
            field=models.CharField(choices=[('SW', 'switch'), ('R', 'router'), ('AP', 'accesspoint'), ('U', 'unknown')], default='U', max_length=30),
        ),
        migrations.AlterField(
            model_name='device',
            name='deviceType',
            field=models.CharField(choices=[('F', 'fix'), ('M', 'modular'), ('AP', 'accesspoint'), ('U', 'unknown')], default='U', max_length=30),
        ),
        migrations.AlterField(
            model_name='device',
            name='role',
            field=models.CharField(choices=[('H', 'host'), ('AP', 'accesspoint'), ('A', 'access'), ('D', 'distribution'), ('C', 'core'), ('U', 'unknown')], default='U', max_length=30),
        ),
    ]
