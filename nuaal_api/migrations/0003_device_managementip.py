# Generated by Django 2.1.1 on 2018-10-18 08:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nuaal_api', '0002_auto_20181018_1021'),
    ]

    operations = [
        migrations.AddField(
            model_name='device',
            name='managementIp',
            field=models.CharField(default='', max_length=15),
        ),
    ]
