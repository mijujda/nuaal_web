from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
import re
from nuaal.Parsers import CiscoIOSParser
import json
from nuaal_api.models import Site, Device, Interface, Link
from nuaal_api.serializers import DeviceSerializer

class Command(BaseCommand):

    def load_file(self, file):
        data = None
        with open(file=file, mode="r") as f:
            data = json.load(fp=f)
        return data

    def get_site(self, sitename=None):
        default_site = Site.objects.get(siteShortName__exact="DEF")
        if sitename:
            query_set = Site.objects.filter(siteShortName__exact=sitename)
            if len(query_set) == 1:
                return query_set.first()
            else:
                return default_site
        else:
            return default_site

    def create_device(self, node_dict):
        exists = self.check_device(node_dict=node_dict)
        if isinstance(exists, Device):
            print("Device with hostname '{}' already exists. Skipping.".format(node_dict["hostname"]))
            """
            exists.hostname = node_dict["hostname"]
            exists.managementIp = node_dict["ipAddress"]
            exists.vendor = node_dict["vendor"] if node_dict["vendor"] else ""
            exists.platform = node_dict["platform"]
            exists.sw = node_dict["software"] if node_dict["software"] else ""
            exists.swVersion = node_dict["version"] if node_dict["version"] else ""
            exists.site = self.get_site()
            exists.deviceFamily = self.get_deviceFamily(node_dict["platform"])
            exists.save()
            """
        elif not exists:
            device = Device(
                hostname=node_dict["hostname"],
                managementIp=node_dict["ipAddress"],
                vendor=node_dict["vendor"] if node_dict["vendor"] else "",
                platform=node_dict["platform"],
                sw=node_dict["software"] if node_dict["software"] else "",
                swVersion=node_dict["version"] if node_dict["version"] else "",
                site=self.get_site(),
                deviceFamily=self.get_deviceFamily(node_dict["platform"])
            )
            print("Adding device with hostname '{}'".format(device.hostname))
            device.save()

    def check_device(self, node_dict):
        query_set = Device.objects.filter(hostname__exact=node_dict["hostname"])
        if len(query_set) > 1:
            print("More than 1 device found with hostname '{}'".format(node_dict["hostname"]))
            query_set = query_set.filter(managementIp__exact=node_dict["ipAddress"])
            if len(query_set) != 1:
                return False
            else:
                return query_set.first()
        elif len(query_set) == 1:
            return query_set.first()
        else:
            return False

    def create_link(self, link_dict):
        exists = self.check_link(link_dict=link_dict)
        exists_reverse = self.check_link(self.get_reverse_link(link_dict=link_dict))
        if isinstance(exists, Link) or isinstance(exists_reverse, Link):
            print("Link exists or reverse - Skipping.")
            pass
            # TODO: Handle link update... ??
        elif (not exists) and (not exists_reverse):
            try:
                sourceDevice = Device.objects.get(hostname__exact=link_dict["sourceNode"])
                targetDevice = Device.objects.get(hostname__exact=link_dict["targetNode"])
                link = Link(
                    sourceDevice=sourceDevice,
                    targetDevice=targetDevice,
                    sourceInterface=link_dict["sourceInterface"],
                    targetInterface=link_dict["targetInterface"]
                )
                link.save()
                print("Added new link: {}".format(str(link)))
            except ObjectDoesNotExist as e:
                print("Cannot add link {}, source/target node does not exist. Exception: {}".format(
                    link_dict,
                    repr(e)
                ))

    def check_link(self, link_dict):
        sourceDevice = Device.objects.filter(hostname__exact=link_dict["sourceNode"]).first()
        targetDevice = Device.objects.filter(hostname__exact=link_dict["targetNode"]).first()
        links = Link.objects.filter(
            sourceDevice=sourceDevice,
            targetDevice=targetDevice,
            sourceInterface__exact=link_dict["sourceInterface"],
            targetInterface__exact=link_dict["targetInterface"]
        )
        if len(links) > 1:
            print("ERROR: Found multiple instances of the same link!")
            for link in links:
                print("DUPLICATE: {} with ID: {}".format(str(link), link.id))
            return True
        elif len(links) == 1:
            return links.first()
        else:
            return False

    def get_reverse_link(self, link_dict):
        reverse_link = {
            "sourceNode": link_dict["targetNode"],
            "sourceInterface": link_dict["targetInterface"],
            "targetNode": link_dict["sourceNode"],
            "targetInterface": link_dict["sourceInterface"]
        }
        return reverse_link

    def get_deviceFamily(self, platform):
        family_map = {
            "WS-C": "switch",
            "AIR-": "accesspoint"
        }
        for prefix, family in family_map.items():
            if str(platform)[:len(prefix)] == prefix:
                return family
        return "unknown"

    def handle(self, *args, **options):
        data = self.load_file(r"C:\Users\mhudec\CloudStation\Work\ALEF\MHMP\scripts\tp2.json")
        for node_dict in data["nodes"]:
            self.create_device(node_dict=node_dict)
        for link_dict in data["links"]:
            self.create_link(link_dict=link_dict)
        pass