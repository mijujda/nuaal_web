from django.core.management.base import BaseCommand
from django.core.serializers import serialize
import xlsxwriter
from nuaal_api.models import Site, Device
import json
import sys

class Command(BaseCommand):

    def get_sites(self):
        sites = serialize("json", Site.objects.all())
        new_sites = []
        for site in json.loads(sites):
            new_sites.append(site["fields"])
        print(new_sites)
        return new_sites

    def get_device_per_site(self, site):
        devices = serialize("json", Device.objects.filter(site__siteShortName__exact=site))
        new_devices = []
        for device in json.loads(devices):
            new_devices.append(device["fields"])
        print(new_devices)
        return new_devices

    def write_worksheet(self, workbook, name, data):
        worksheet = workbook.add_worksheet(name=name)
        bold = workbook.add_format({"bold": True})
        if len(data) > 0:
            headers = list(data[0].keys())
            if "site" in headers:
                headers.remove("site")
            row_pointer = 0
            collumn_pointer = 0
            worksheet.write_row(row_pointer, collumn_pointer, headers, bold)
            row_pointer += 1
            for entry in data:
                line = []
                for head in headers:
                    line.append(entry[head])
                worksheet.write_row(row_pointer, collumn_pointer, line)
                row_pointer += 1

    def handle(self, *args, **options):
        sites = self.get_sites()
        site_names = []
        for site in sites:
            site_names.append(site["siteShortName"])
        workbook = xlsxwriter.Workbook("excel_dump.xlsx")
        self.write_worksheet(workbook=workbook, name="Sites", data=sites)
        for site in site_names:
            devices = self.get_device_per_site(site=site)
            self.write_worksheet(workbook=workbook, name=site, data=devices)
        try:
            workbook.close()
        except PermissionError:
            user_input = None
            while user_input not in ["y", "Y", "n", "N"]:
                print("The file is probably opened. Please close it.")
                user_input = input("Did you close the file? [y/n]")
                if user_input in ["y", "Y"]:
                    workbook.close()
                elif user_input in ["n", "N"]:
                    print("Could not write changes to the file.")
                    sys.exit(1)
                else:
                    continue

