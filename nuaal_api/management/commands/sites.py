from django.core.management.base import BaseCommand
import json
from nuaal_api.models import Site, Device, Interface
from nuaal_api.serializers import DeviceSerializer

class Command(BaseCommand):

    def handle(self, *args, **options):
        sites = """BOH;DSA - STK Bohdalec;Nad Vršovskou horou 88/4, Praha
BREV;DSA - STK Břevnov;Radimova 39, Praha 6
BUB;Archiv Bubny;Bubenská 8a, Praha
DC1;DC1 - VEGACOM;Lužná 4/591, Praha 6
DUM;Dům národních menšin;Vocelova 3, Praha 2
EMA;OMI Emauzy;Vyšehradská 55, Praha 2
CHAR;MHMP - ZSP;Charvátova 9/145, Praha 1
JAR;DSA - STK Jarov / Osiková;Osiková 2688/2, Praha 3
JUN;MHMP - Škodův palác;Jungmannova 25, Praha 1
KLA;MHMP - Clam-Gallasův palác;Mariánské nám. 3, Praha
KON;DSA - KCP (Kongresové Centrum Praha);Na Pankráci 1685/17, Praha 4
MKP;Městská knihovna v Praze (MKP);Mariánské nám. 1, Praha
NR;MHMP - Nová radnice;Mariánské náměstí 2, Praha 1
NUB;MHMP - Nová úřední budova;Nám. Franze Kafky 1, Praha 1
OPLET;MHMP - Opletalova;Opletalova 22, Praha 1
RAB;MHMP Radniční blok (minuta); U Radnice, Praha
REZ;MHMP - Primátorská rezidence;Mariánské nám. 1, Praha
RYT;MHMP Rytířská;Rytířská 10, Praha 1
STR;MHMP-Staroměstská radnice;Staroměstské náměstí 1/4, Praha
VAL;MHMP - Valentinská;Valentinská 4, Praha
VYS;DSA - STK Vysočany;Na výběžku 688/11, Praha 9"""
        sites = (x for x in sites.split("\n"))
        parsed_sites = [x.split(";") for x in sites]
        for site in parsed_sites:
            a = Site(siteShortName=site[0], siteLongName=site[1], address=site[2])
            a.save()