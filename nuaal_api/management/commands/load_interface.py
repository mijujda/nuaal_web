from django.core.management.base import BaseCommand
import json
from nuaal_api.models import Device, Interface
from nuaal.utils import int_name_convert, interface_split

class Command(BaseCommand):

    def get_device(self, hostname):
        device = None
        try:
            device = Device.objects.get(hostname=hostname)
        except Exception as e:
            print("ERROR: Failed to find device with hostname: {}. Exception: {}".format(hostname, repr(e)))
        finally:
            return device

    def get_interfaceFamily(self, interface_name):
        interface_name = interface_split(interface_name)[0]
        physical_int = ["Ethernet", "Serial", "FastEthernet", "GigabitEthernet", "TenGigabitEthernet"]
        virtual_int = ["Tunnel", "Vlan", "Loopback"]
        portchannel_int = ["Port-channel"]
        if interface_name in physical_int:
            return "physical"
        elif interface_name in virtual_int:
            return "virtual"
        elif interface_name in portchannel_int:
            return "etherchannel"
        else:
            return "unknown"


    def add_interface(self, device, interface_dict):
        interface_name = int_name_convert(interface_dict["interface"])
        interface = Interface(
            nameLong=interface_name,
            nameShort=interface_dict["interface"],
            description=interface_dict["name"],
            interfaceFamily=self.get_interfaceFamily(interface_name),
            interfaceType=interface_dict["type"],
            device=device
        )
        interface.save()

    def handle(self, *args, **options):
        data = None
        with open(file=r"C:\Users\mhudec\CloudStation\Work\ALEF\MHMP\scripts\intStatus.json", mode="r") as f:
            data = json.load(fp=f)
        for hostname, interfaces in data.items():
            device = self.get_device(hostname=hostname)
            if isinstance(device, Device):
                for interface in interfaces:
                    self.add_interface(device=device, interface_dict=interface)