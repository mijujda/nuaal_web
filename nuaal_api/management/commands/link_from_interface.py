from django.core.management.base import BaseCommand
from nuaal_api.models import Link, Interface

class Command(BaseCommand):

    def update_links(self):
        unknown_links = []
        links_qs = Link.objects.all()
        for link in links_qs:
            interfaces = link.get_interfaces()
            for i in interfaces:
                if "BaseT" in i.interfaceType:
                    link.linkType = "base-t"
                    link.save()
                    break
                elif "CX SFP Cable" in i.interfaceType:
                    link.linkType = "twinax"
                    link.save()
                    break
                elif "BaseSX" in i.interfaceType or "ase-SR" in i.interfaceType:
                    link.linkType = "multimode"
                    link.save()
                    break
                elif "BaseLX" in i.interfaceType or "BaseLH" in i.interfaceType or "ase-LR" in i.interfaceType:
                    link.linkType = "singlemode"
                    link.save()
                    break
            if link.linkType == "unknown":
                unknown_links.append(link)
                print("Could not determine linkType for link: {} (ID:{})".format(link,link.id))
        for link in unknown_links:
            print(link, end="\t")
            interfaces = link.get_interfaces()
            for i in interfaces:
                print(i.interfaceType, end="\t")
            print()


    def handle(self, *args, **options):
        self.update_links()
