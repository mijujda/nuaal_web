from django.core.management.base import BaseCommand
import json
from nuaal_api.models import Site, Device, Interface
from nuaal_api.serializers import DeviceSerializer

class Command(BaseCommand):
    def get_default_site(self, site_id):
        site = None
        if site_id:
            try:
                site = Site.objects.get(id=site_id)
            except Exception as e:
                site = Site.objects.filter(siteShortName__exact="DEF")
            finally:
                return site
        else:
            return Site.objects.filter(siteShortName__exact="DEF").first()

    def create_device_from_json(self, data):
        if isinstance(data, dict):
            pass
        if isinstance(data, str):
            data = json.loads(data)
        if not self.device_exists(hostname=data["hostname"]):
            data["site"] = self.get_default_site(site_id=data["site"])
            new_device = Device(**data)
            new_device.save()
        else:
            print("Device with hostname {} already exists.".format(data["hostname"]))


    def device_exists(self, hostname):
        query_set = Device.objects.filter(hostname__exact=hostname)
        if len(query_set) == 0:
            return False
        if len(query_set) == 1:
            return query_set[0]

    def handle(self, *args, **options):
        device = {
            "hostname": "cat001nr",
            "managementIp": "10.16.14.65",
            "platform": "WS-C2960X-48TS-L",
            "vendor": "Cisco",
            "sw": "IOS",
            "swVersion": "15.2(2)E5",
            "site": "2f885b6e2fea462a842d1a01586eed38"
          }
        self.create_device_from_json(device)
