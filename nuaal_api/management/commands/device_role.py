from django.core.management.base import BaseCommand
from nuaal_api.models import Site, Device

class Command(BaseCommand):

    def handle(self, *args, **options):
        sites = Site.objects.all()
        for site in sites:
            site_counter = 0
            devices = site.device_set.all()
            for device in devices:
                site_counter += device.get_neighbors_count()
            site_average = 0
            try:
                site_average = site_counter / len(devices)
            except ZeroDivisionError:
                pass
            finally:
                print("Average for site {}: {}".format(site.siteShortName, site_average))
