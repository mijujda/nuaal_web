from django.contrib import admin
from .models import Device, Interface, Site, Link

# Register your models here.
@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    pass

@admin.register(Interface)
class InterfaceAdmin(admin.ModelAdmin):
    pass

@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    pass

@admin.register(Link)
class SiteAdmin(admin.ModelAdmin):
    pass