from rest_framework import serializers
from .models import Device, Interface, Site, Link


class DeviceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Device
        fields = '__all__'


class InterfaceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Interface
        fields = '__all__'


class SiteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Site
        fields = '__all__'


class LinkSerializer(serializers.ModelSerializer):

    class Meta:
        model = Link
        fields = '__all__'
