from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from nuaal_api import views


urlpatterns = [
    path('device/', views.DeviceList.as_view()),
    path('device/<pk>/', views.DeviceDetail.as_view()),
    path('interface/', views.InterfaceList.as_view()),
    path('site/', views.SiteList.as_view()),
    path('site/<pk>/', views.SiteDetail.as_view()),
    path('link/', views.LinkList.as_view()),
    path('link/<pk>/', views.LinkDetail.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)