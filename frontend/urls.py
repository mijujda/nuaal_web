from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from frontend import views


urlpatterns = [
    path('', views.index),
    path('device/', views.devices),
    path('newdevice/', views.devices_new)
]

urlpatterns = format_suffix_patterns(urlpatterns)