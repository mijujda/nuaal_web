from django.shortcuts import render, HttpResponse
from django.template.loader import render_to_string
from nuaal_api.models import Device
import os
import json

# Create your views here.
def index(request):
    context = Device.objects.all()
    return render(request=request, template_name="frontend/index.html", context={"devices": context})

def devices(request):
    device_qs = Device.objects.all()
    context = {"devices": device_qs}
    content_string = render_to_string(template_name="frontend/data_json.jinja2", context=context)
    filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "static", "frontend", "data.json")
    with open(file=filepath, mode="w") as f:
        f.write(content_string)
    return render(request=request, template_name="frontend/devices.html", context=context)

def devices_new(request):
    return render(request=request, template_name="frontend/new_devices.html")